package itacademy.com.project023;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private NotificationManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        findViewById(R.id.btnDelete).setOnClickListener(this);
        findViewById(R.id.btnChange).setOnClickListener(this);

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_play_arrow_black_24dp)
                .setContentTitle("new notification")
                .setContentText("new title")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(1, builder.build());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDelete:
                manager.cancel(1);
                break;
            case R.id.btnChange:
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_play_arrow_black_24dp)
                        .setContentTitle("UPDATED TITLE")
                        .setContentText("UPDATED TEXT");

                Notification notification = builder.build();

                manager.notify(1, notification);
                break;
        }
    }
}
