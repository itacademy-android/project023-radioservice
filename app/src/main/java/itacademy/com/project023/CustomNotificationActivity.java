package itacademy.com.project023;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RemoteViews;

public class CustomNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private NotificationManager manager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        findViewById(R.id.btnCreate).setOnClickListener(this);
        findViewById(R.id.btnChange).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnChange:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.btnCreate:
                show();
                break;
        }
    }

    private void show() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.note_expanded);
        remoteViews.setTextViewText(R.id.tvSongTitle, "Test title");

        Intent intent = new Intent(this, CustomNotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent playIntent = new Intent(this, RadioService.class);
        playIntent.setAction("changeRadioState");
        PendingIntent pIntentPlay = PendingIntent.getService(this, 0,
                playIntent, 0);

        remoteViews.setOnClickPendingIntent(R.id.btnPlay, pIntentPlay);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContent(remoteViews)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setOngoing(true);

        manager.notify(1, builder.build());

    }
}
