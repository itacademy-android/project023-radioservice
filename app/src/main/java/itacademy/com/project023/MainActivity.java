package itacademy.com.project023;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        AdapterView.OnItemClickListener,
        View.OnClickListener {

    private ListView lvRadio;

    private ImageButton btnPlay;

    private RadioService service;

    private boolean isBound;

    private SlidingUpPanelLayout playerPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvRadio = findViewById(R.id.lvRadio);

        playerPanel = findViewById(R.id.playerPanel);

        btnPlay = findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(this);

        String[] radioList = getResources().getStringArray(R.array.radio_array);

        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (this, android.R.layout.simple_list_item_2, android.R.id.text1, radioList);

        lvRadio.setAdapter(adapter);

        lvRadio.setOnItemClickListener(this);

        Intent intent = new Intent(this, RadioService.class);

        bindService(intent, connection, BIND_AUTO_CREATE);

    }

    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            isBound = true;

            RadioService.RadioBinder binder = (RadioService.RadioBinder) iBinder;
            service = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBound = false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound) unbindService(connection);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        service.playRadio(adapterView.getItemAtPosition(i).toString());

        btnPlay.setBackgroundResource(R.drawable.ic_pause_black_24dp);

        playerPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
    }

    @Override
    public void onClick(View view) {
        if (!service.handleRadioPlay()) {
            btnPlay.setBackgroundResource(R.drawable.ic_play_arrow_black_24dp);
        } else {
            btnPlay.setBackgroundResource(R.drawable.ic_pause_black_24dp);
        }
    }
}
