package itacademy.com.project023;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;

public class RadioService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {

    private MediaPlayer player;

    private RadioBinder binder = new RadioBinder();

    @Override
    public void onCreate() {
        super.onCreate();

        player = new MediaPlayer();
        player.setOnErrorListener(this);
        player.setOnPreparedListener(this);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();

        if (action != null && action.equals("changeRadioState")) {
            handleRadioPlay();
        }

        return START_NOT_STICKY;
    }

    public class RadioBinder extends Binder {
        public RadioService getService() {
            return RadioService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.release();
        return super.onUnbind(intent);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        player.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }

    public void playRadio(String radio) {
        player.reset();

        Uri uri = Uri.parse(radio);

        if (uri == null) {
            Log.e("SERVICE_ERROR", "URI_CANNOT_BE_EMPTY");
            return;
        }

        try {
            player.setDataSource(getApplicationContext(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.prepareAsync();
    }

    public boolean handleRadioPlay() {
        if (player.isPlaying() || player.isLooping()) {
            player.pause();
            return false;
        }
        player.start();
        return true;
    }
}
